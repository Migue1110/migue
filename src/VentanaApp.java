import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;

public class VentanaApp {

	protected Shell shell;
	private Text txtMigue;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			VentanaApp window = new VentanaApp();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblHola = new Label(shell, SWT.NONE);
		lblHola.setBounds(10, 10, 55, 15);
		lblHola.setText("Hola");
		
		txtMigue = new Text(shell, SWT.BORDER);
		txtMigue.setText("migue");
		txtMigue.setBounds(38, 68, 76, 21);

	}
}
